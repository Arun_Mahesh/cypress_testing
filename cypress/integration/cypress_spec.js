describe("CS tests", () => {
  it("Website test", () => {
    cy.visit(
      "https://docs.cypress.io/guides/overview/why-cypress.html#In-a-nutshell"
    );
    cy.wait(2000);
    cy.get("strong").contains("Getting Started").click();
    cy.wait(1000);
    cy.get("a").contains("Testing Your App").click();
    cy.get("a").contains("opened the Test Runner").click();
    cy.wait(1000);
    cy.get("strong").contains("Dashboard").click();
    cy.get("a").contains("Organizations").click();
    cy.get("a").contains("Dashboard Service").click();
    cy.get("a").contains("Single Sign-On").click();
  });
});
