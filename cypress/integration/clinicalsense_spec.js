describe("Patient listing generator tests", () => {
  it("PLG test", () => {
    cy.visit("https://cs-dedr2.statwb.eu.novartis.net/accounts/login");
    cy.wait(10000);
    cy.get("#cs-auth-form_username").type(Cypress.env("login"));
    cy.get("#cs-auth-form_password").type(Cypress.env("password"), {
      log: false,
    });
    cy.wait(10000);
    cy.get("button").click();
    cy.wait(1000);
    cy.visit("https://cs-dedr2.statwb.eu.novartis.net/");
    cy.wait(2000);
    });

   
    it("Portfolio test", () => {
      cy.get("span").contains("Create a Portfolio").parent().click();
      cy.wait(5000);
      cy.get("input.ant-select-search__field").first().click();
      cy.get("input.ant-select-search__field").first().type("CAIN457A").blur();
      cy.wait(5000);
      cy.get("input.ant-select-search__field").eq(1).click();
      cy.get("input.ant-select-search__field").eq(1).type("CAIN457A1302").blur();
      cy.wait(5000);
      cy.get("input.ant-select-search__field").eq(2).click();
      cy.get("input.ant-select-search__field").eq(2).type("csr_1").blur();
    });
});
